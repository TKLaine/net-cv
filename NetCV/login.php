<?php
   include("php\serv_config.php");
   session_start();

   if($_SERVER["REQUEST_METHOD"] == "POST") {
      //Tähän tulee lomakkeelta tulleet arvot. Syötetyt arvot tarkastetaan injektioden varalta

      $myusername = mysqli_real_escape_string($db,$_POST['accountname']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']);
      $error = "";

      $sql = "SELECT Password FROM account WHERE AccountName = '$myusername'";
      $result = mysqli_query($db,$sql);
      $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
      $password_hashed = $row['Password'];



       if (password_verify($mypassword, $password_hashed)) {
           // Correct password
           $_SESSION['login_user'] = $myusername;
           $idsql = "SELECT UserID FROM account WHERE AccountName = '$myusername'";
           $result = mysqli_query($db, $idsql);
           $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
           $_SESSION['myID'] = $row['UserID'];
           header("location: edit.html");
       } else {
           // Incorrect password
           $error = "Sisäänkirjautuminen väärin, tarkista käyttäjätunnus ja salasana";
       }
   }
?>
<html>

<head>
    <title>Sisäänkirjautuminen</title>

    <link href="./css/login_style.css" rel="stylesheet" type="text/css">



</head>

<body>
<h1 class="center">Net-CV</h1>
<div class="center">

    <div class="bigbox">

        <div class="labelbox"><b>Sisäänkirjautuminen</b></div>

        <div  class="labelbox2">

            <form method = "post">
                <label class="label">Käyttäjänimi  :</label><input type = "text" class = "box" name = "accountname" /><br /><br />
                <label class="label">Salasana  :</label><input type = "password" class = "box" name = "password"  /><br/><br />
                <input type = "submit" value = " Kirjaudu "/>
                <input type="button" value="Luo uusi tili" onclick="window.location.href = 'register.php';"></input>
            </form>

            <div class="error">
                <?php

                echo $error; ?></div>

        </div>

    </div>

</div>


</body>
</html>