'use strict';
window.onload = function () {
    let etunimi = document.getElementById('nimi');
    let syntaika = document.getElementById('syntaika');
    let email = document.getElementById('email');
    let image = document.getElementById('image');
    let fields = [etunimi, syntaika, email];


    let message;
    let xhr = new XMLHttpRequest();
    let jsonReceived = true;

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if (xhr.responseText == false) {
                jsonReceived = false;
            } else {
                let parsedResponse = JSON.parse(xhr.responseText);

                //Tulostetaan perustiedot
                if (parsedResponse['user'][0]['ImageLink'] == "") {

                    image.src = "./img/default.jpg";
                }else{

                    image.src = parsedResponse['user'][0]['ImageLink'];
                }
                if (parsedResponse.user == null) {
                    for (let x in fields) {
                        fields[x].innerText = "";
                    }

                }
                else {
                    message = parsedResponse['user'][0]['FirstName'] + " " + parsedResponse['user'][0]['LastName'];
                    etunimi.innerHTML = message;
                    message = parsedResponse['user'][0]['BirthDate'];
                    syntaika.innerHTML = message;
                    message = parsedResponse['user'][0]['Email'];
                    email.innerHTML = message;
                }


                //Tästä eteenpäin täytetään työkokemus ja tutkintotaulut oikeilla tiedoilla
                let workRow = [];
                let workRows = [];

                //Tungetaan työkokemukset omaan taulukkoonsa
                for (let l in parsedResponse['workexperience']) {
                    workRow.push(parsedResponse['workexperience'][l]['RelevantDate'],
                        parsedResponse['workexperience'][l]['Institute'],
                        parsedResponse['workexperience'][l]['FreeDescription']);
                    workRows[l] = workRow;
                    workRow = [];
                }

                //lisätään kaikki työkokemusrivit otsikoineen

                let th = document.createElement("TH");
                textnode = document.createTextNode("Työkokemukset: ");
                th.appendChild(textnode);
                document.getElementById("kokemukset").appendChild(th);

                var tr = document.createElement("TR");
                document.getElementById("kokemukset").appendChild(tr);

                var td = document.createElement("TD");
                textnode = document.createTextNode("Ajankohta: ");
                td.appendChild(textnode);
                document.getElementById("kokemukset").appendChild(td);

                var td = document.createElement("TD");
                textnode = document.createTextNode("Työnantaja: ");
                td.appendChild(textnode);
                document.getElementById("kokemukset").appendChild(td);

                var td = document.createElement("TD");
                textnode = document.createTextNode("Kuvaus: ");
                td.appendChild(textnode);
                document.getElementById("kokemukset").appendChild(td);

                var tr = document.createElement("TR");
                document.getElementById("kokemukset").appendChild(tr);

                //Lisätään työkokemusrivit
                for (let y = 0; y < workRows.length; y++) {

                    for (let x = 0; x < workRows[y].length; x++) {

                        var td = document.createElement("TD");
                        var textnode = document.createTextNode(workRows[y][x]);
                        td.appendChild(textnode);
                        document.getElementById("kokemukset").appendChild(td);

                    }

                    var tr = document.createElement("TR");

                    document.getElementById("kokemukset").appendChild(tr);


                }


                //Tungetaan koulutukset omaan taulukkoonsa
                let degreeRow = [];
                let degreeRows = [];

                for (let l in parsedResponse['degrees']) {

                    degreeRow.push(parsedResponse['degrees'][l]['RelevantDate'],
                        parsedResponse['degrees'][l]['Institute'],
                        parsedResponse['degrees'][l]['FreeDescription']);
                    degreeRows[l] = degreeRow;
                    degreeRow = [];
                }


                //lisätään kaikki koulutusrivit otsikoineen

                var tr = document.createElement("TR");
                document.getElementById("kokemukset").appendChild(tr);

                th = document.createElement("TH");
                var textnode = document.createTextNode("Koulutukset: ");
                th.appendChild(textnode);
                document.getElementById("kokemukset").appendChild(th);

                var tr = document.createElement("TR");
                document.getElementById("kokemukset").appendChild(tr);

                var td = document.createElement("TD");
                textnode = document.createTextNode("Ajankohta: ");
                td.appendChild(textnode);
                document.getElementById("kokemukset").appendChild(td);

                var td = document.createElement("TD");
                textnode = document.createTextNode("Suorituspaikka: ");
                td.appendChild(textnode);
                document.getElementById("kokemukset").appendChild(td);

                var td = document.createElement("TD");
                textnode = document.createTextNode("Kuvaus: ");
                td.appendChild(textnode);
                document.getElementById("kokemukset").appendChild(td);


                var tr = document.createElement("TR");
                document.getElementById("kokemukset").appendChild(tr);

                //Lisätään koulutusrivit

                for (let y = 0; y < degreeRows.length; y++) {

                    for (let x = 0; x < degreeRows[y].length; x++) {

                        var td = document.createElement("TD");
                        var textnode = document.createTextNode(degreeRows[y][x]);
                        td.appendChild(textnode);
                        document.getElementById("kokemukset").appendChild(td);

                    }

                    var tr = document.createElement("TR");
                    document.getElementById("kokemukset").appendChild(tr);

                }

            }
        }
    }

    xhr.open("GET", "http://localhost:80/NetCV/netcv/user", true);
    xhr.send();
};