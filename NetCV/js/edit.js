'use strict';
//Author Torsti Laine
window.onload = function () {
    let lisaa = document.getElementById('lisaa');
    let muokkaa = document.getElementById('muokkaa');
    let lisaahenkilotiedot = document.getElementById('lisaahenkilotiedot');
    let muokkaakentta = document.getElementById('muokkaakentta');

    let jsonReceived = true;
    let addNewRow = false;
    let toTable = "";

    updateAllListContent();
    populateListAdd();

    //erases content from the Preview field
    function clearPreviewField(){
        let previewField = document.getElementById("esikatselu");
        if(previewField.hasChildNodes()) {
            while (previewField.hasChildNodes()) {
                previewField.removeChild(previewField.childNodes[0]);
            }
        }
    }

    //create content for the 'Lisää Kenttä'
    function populateListAdd (){
        let unorderedListAdd = document.querySelector('#lisaa ul');
        let presetDataFields = Array("tutkinto", "työkokemus");
        for(let item in presetDataFields){
            let linkNameAdd = document.createTextNode("Lisää " + presetDataFields[item]);
            let linkAdd = document.createElement("a");
            linkAdd.setAttribute("href", "#lisaa" + presetDataFields[item]);
            switch(presetDataFields[item]) {
                case "tutkinto":
                    linkAdd.onclick = function (){
                        console.log("tutkinto");
                        document.querySelector('#muokkaakentta input:nth-Child(1)').value = "";
                        document.querySelector('#muokkaakentta input:nth-Child(2)').value = "";
                        document.querySelector('#muokkaakentta textarea').value = "";
                        toTable = "degree";
                        addNewRow = true;
                        muokkaakentta.style.display = 'flex';
                        lisaahenkilotiedot.style.display = 'none';
                    };
                    break;
                case "työkokemus":
                    linkAdd.onclick = function (){
                        console.log("työkokemus");
                        toTable = "workexperience";
                        addNewRow = true;
                        muokkaakentta.style.display = 'flex';
                        lisaahenkilotiedot.style.display = 'none';
                    };
                    break;
                default:
                    linkAdd.onclick = function (){
                        console.log("deafult")
                    };
            }
            let liListAdd = document.createElement("li");
            linkAdd.appendChild(linkNameAdd);
            liListAdd.appendChild(linkAdd);
            unorderedListAdd.appendChild(liListAdd);
        }
    }

    //creates a link where a new user can insert the initial information regarding themselves
    function newAccount(){
        //If no json was received by updateAllListContent() this function is called
        jsonReceived = false;
        let addList = document.querySelector("#lisaa ul");
        let content = document.createTextNode("Lisää henkilötiedot");
        let link = document.createElement("a");
        link.setAttribute("href", "#henkilotiedot");
        link.onclick = function(){
            muokkaakentta.style.display = 'none';
            lisaahenkilotiedot.style.display = 'flex';
        };
        link.appendChild(content);
        let listItem = document.createElement("li");
        listItem.setAttribute("id", "lisaahenkilotiedot");
        listItem.appendChild(link);
        addList.insertBefore(listItem, addList.childNodes[0])
    }

    //allows a list to hide its children as part of it's onclick event
    function hideShowChildren(element){
        let childNodes = element.children;
        for(let i = 0; i < childNodes.length; i++){
            if(childNodes[i].style.display == 'none') {
                childNodes[i].style.display = 'block';
            }else{
                childNodes[i].style.display = 'none';
            }
        }
    }

    // extract the names of 1st tier elements of a JSON
    function getFirstTierFieldNames(jsonParsedResponse){
        let firstTierFieldNames = [];
        for (let name in jsonParsedResponse){
            firstTierFieldNames.push(name);
        }
        return firstTierFieldNames;
    }

    // extract the names of 1st tier elements of a JSON
    function getSecondTierFieldNames(firstTierFieldName){
        let secondTierFieldNames = [];
        for (let secondTierFieldName in firstTierFieldName[0]){
            secondTierFieldNames.push(secondTierFieldName);
        }
        return secondTierFieldNames;

    }

    // extract the names of instituotions such as emplyer or school from the JSON fields
    function getInstituteNames(dataFieldValues, dataFieldNames){
        let institutes = [];
        let n = 0;
        for(let i = 0; i < (dataFieldValues.length/dataFieldNames.length); i++){
            for(let j = 0; j < dataFieldNames.length; j++){
                if (dataFieldNames[j] == "Institute" && !institutes.includes(dataFieldValues[n])) {
                    institutes.push(dataFieldValues[n])
                }
                n++;
            }
        }
        return institutes;
    }

    // extract the values from a JSON object
    function getDataFieldValues(jsonParsedDataset, dataFieldNames) {
        let dataFieldValues =[];
        for (let i = 0; i < jsonParsedDataset.length;i++){
            for (let j = 0; j < dataFieldNames.length; j++) {
                dataFieldValues.push(jsonParsedDataset[i][dataFieldNames[j]]);
            }
        }
        return dataFieldValues;
    }

    //creates an <li> element with an embedded <a> link or creates lists with multiple <li> embedded links
    function createElementWithLink(element, dataFieldNames, dataFieldValues, user){
        let newLink = document.createElement("a");
        newLink.setAttribute("href", "#" + element);
        newLink.innerHTML = element;
        let n = 0;
        if(user) {
            newLink.onclick = function () {
                for (let i = 0; i < (dataFieldValues.length / dataFieldNames.length); i++) {
                    for (let j = 0; j < dataFieldNames.length; j++) {
                        if (dataFieldNames[j] != "UserID" && dataFieldValues[n] != "") {
                            document.getElementById(`${dataFieldNames[n]}`).value = dataFieldValues[n];
                        }
                        n++;
                    }
                }
                lisaahenkilotiedot.style.display = 'flex';
                muokkaakentta.style.display = 'none';
            };
            return newLink;
        }else {
            // Create a multidimensional list with links
            // we create a new 1st tier list item
            let newFirstTierListItem = document.createElement("li");
            // we create a new 1st tier unordered list
            let newFirstTierUnorderedList = document.createElement("ul");
            // we add functionality to the 1st tier link
            newLink.onclick = function () {
                hideShowChildren(newFirstTierUnorderedList);
            };
            // we append the 1st tier link to the 1st tier unordered list -> <ul><a href="element">element</a></ul>
            newFirstTierListItem.appendChild(newLink);
            //Create the <li (2nd Tier)> elements with links to show third tier elements to append to the <ul>
            let instituteNames = getInstituteNames(dataFieldValues, dataFieldNames);
            for (let i = 0; i < instituteNames.length; i++) {
                // we create a new 2nd tier list item that will contain the 2nd level un ordered list
                let newSecondTierListItem = document.createElement("li");
                // we create a new 2nd tier unordered list that will contain 3rd tier list items
                let newSecondTierUnorderedList = document.createElement("ul");
                let newSecondTierLink = document.createElement("a");

                newSecondTierLink.innerHTML = instituteNames[i];
                newSecondTierLink.setAttribute("href", "#" + instituteNames[i]);
                newSecondTierLink.onclick = function () {
                    hideShowChildren(newSecondTierUnorderedList);
                };
                newSecondTierListItem.appendChild(newSecondTierLink);

                for (let j = 0; j < dataFieldValues.length; j++) {
                    if (dataFieldValues[j] == instituteNames[i]) {
                        let newThirdTierListItem = document.createElement("li");
                        let newThirdTierLink = document.createElement("a");
                        newThirdTierLink.innerHTML = dataFieldValues[j-1];
                        newThirdTierLink.onclick = function () {
                            muokkaakentta.style.display = 'flex';
                            lisaahenkilotiedot.style.display = 'none';
                            let thisElement = element;
                            document.getElementById("RelevantDate").value = dataFieldValues[j-1];
                            document.getElementById("Institute").value = dataFieldValues[j];
                            document.getElementById("FreeDescription").value = dataFieldValues[j+1];
                            document.getElementById("TableID").value = dataFieldValues[j+3];
                            if (thisElement == "degrees") {
                                toTable = "degree";
                            } else if (thisElement == "workexperience") {
                                toTable = "workexperience";
                            }
                            addNewRow = false;
                        };
                        newThirdTierListItem.appendChild(newThirdTierLink);
                        newThirdTierListItem.style.display = 'none';
                        // we append the new 3rd tier list item to the 2nd tier unordered list
                        newSecondTierUnorderedList.appendChild(newThirdTierListItem);
                    }
                }
                // we append the second tier unordered list to 1st tier list item
                newSecondTierListItem.appendChild(newSecondTierUnorderedList);
                newSecondTierListItem.style.display = 'none';

                // we append the 2nd tier list item to 1st tier unordered list
                newFirstTierUnorderedList.appendChild(newSecondTierListItem);
            }
            // we append the 1st tier unordered list to the 1st tier list item
            newFirstTierListItem.appendChild(newFirstTierUnorderedList);
            // we return the finished list
            return newFirstTierListItem;
        }
    }

    //updates the user information contained within the list items
    function updateAllListContent(){
        let presetListNames = ["account","user", "degrees", "workexperience"];
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function(){
            if(xhr.readyState == 4 && xhr.status == 200){
                if(xhr.responseText == false){
                    newAccount();
                } else {
                    // first we remove the old content
                    let oldContent = document.getElementById("cvcontent");
                    while(oldContent.hasChildNodes()){
                        oldContent.removeChild(oldContent.childNodes[0]);
                    }
                    // then we insert the updated content
                    let parsedResponse = JSON.parse(xhr.responseText);
                    let firstTierFieldNames = getFirstTierFieldNames(parsedResponse);
                    let masterList = document.getElementById('cvcontent');
                    for(let i = 0; i < firstTierFieldNames.length; i++){
                        if(!presetListNames.includes(firstTierFieldNames[i])){
                            // this is where custom tables will be handled *not implemented*
                        } else if (firstTierFieldNames[i] == "user"){
                            //extract SecondTierField names and values from the JSON
                            let dataFieldNames = getSecondTierFieldNames(parsedResponse[firstTierFieldNames[i]]);
                            let dataFieldValues = getDataFieldValues(parsedResponse[firstTierFieldNames[i]], dataFieldNames);
                            //create new a new list item carrying the dataset's name and assign and ID to it ("u" + dataset's name)
                            let editContentNewListItem = document.createElement("li");

                            editContentNewListItem.setAttribute("id", "" + name);
                            editContentNewListItem.appendChild(createElementWithLink(firstTierFieldNames[i], dataFieldNames, dataFieldValues, true));
                            masterList.appendChild(editContentNewListItem);
                        } else if (name != "account"){
                            //extract secondTierField names and values from the JSON
                            let dataFieldNames = getSecondTierFieldNames(parsedResponse[firstTierFieldNames[i]]);
                            let dataFieldValues = getDataFieldValues(parsedResponse[firstTierFieldNames[i]], dataFieldNames);
                            //create new a new list item carrying the dataset's name, and append it to the masterList
                            masterList.appendChild(createElementWithLink(firstTierFieldNames[i], dataFieldNames, dataFieldValues, false));
                        }
                    }
                }
            }
        };
        xhr.open("GET", "http://localhost:80/NetCV/netcv/user", true);
        xhr.send();
        clearPreviewField();
    }

    //creates the preview field for table information such as education or work xp
    document.querySelector("#muokkaakentta input[name='esikatselu']").onclick = function(){
        clearPreviewField();
        let esikatselu = document.getElementById('esikatselu');
        let newTable = document.createElement('table');
        let tableHeader = document.createElement('thead');
        let tableHead = document.createElement('th');
        let tableName = document.createTextNode("Esikatselu");
        let tableRow = document.createElement('tr');
        tableHead.appendChild(tableName);
        tableHeader.appendChild(tableHead);
        newTable.appendChild(tableHeader);
        for(let i = 0; i < 3; i++){
            let insert;
            if(i==2){
                insert = document.querySelector("#muokkaakentta textarea").value;
            }else{
                insert = document.querySelector("#muokkaakentta input:nth-child(" + (i+1) + ")").value;
            }
            console.log(insert);
            let newDataCell = document.createElement('td');
            let cellContent = document.createTextNode(insert);
            newDataCell.appendChild(cellContent);
            tableRow.appendChild(newDataCell);
        }
        newTable.appendChild(tableRow);
        esikatselu.appendChild(newTable);
    };

    //creates a preview field for personal information
    document.querySelector("#lisaahenkilotiedot input[name='esikatselu']").onclick = function(){
        clearPreviewField();
        let esikatselu = document.getElementById('esikatselu');
        let taulukko = document.createElement('table');
        taulukko.setAttribute('id', 'henkilotiedot');
        taulukko.setAttribute('class', 'tiedot');
        let taulukkootsake = document.createElement('thead');
        let taulukonnimi = document.createElement('th');
        let textnode = document.createTextNode("Henkilötiedot");
        taulukonnimi.appendChild(textnode);
        taulukkootsake.appendChild(taulukonnimi);
        taulukko.appendChild(taulukkootsake);
        let tiedot =["Sukunimi", "Etunimi", "Syntymäaika", "Email"];
        for(let i = 1; i < 5; i++){
            let insert = document.querySelector("#lisaahenkilotiedot input:nth-child(" + i + ")").value;
            let newline = document.createElement('tr');
            let newfieldname = document.createElement('td');
            let name = document.createTextNode(tiedot[i-1]);
            newfieldname.appendChild(name);
            newline.appendChild(newfieldname);
            let newfield = document.createElement('td');
            let fielddata = document.createTextNode(insert);
            newfield.appendChild(fielddata);
            newline.appendChild(newfield);
            taulukko.appendChild(newline);
        }
        esikatselu.appendChild(taulukko);
    };

    //switch edit/add field visibility from muokkaa(edit) to lisaa(add)
    document.getElementById('lisaakentta').onclick = function(){
        console.log('lisaakentta');
        document.querySelector('#muokkaakentta input:nth-Child(1)').value = "";
        document.querySelector('#muokkaakentta input:nth-Child(2)').value = "";
        document.querySelector('#muokkaakentta textarea').value = "";
        console.log('lisaakentta');
        lisaa.style.display = 'block';
        muokkaa.style.display = 'none';
    };

    //switch edit/add field visibility from lisaa(add) to muokkaa(edit)
    document.getElementById('muokkaakenttaa').onclick = function(){
        console.log("muokkaa kenttää");
        document.querySelector('#muokkaakentta input:nth-Child(1)').value = "";
        document.querySelector('#muokkaakentta input:nth-Child(2)').value = "";
        document.querySelector('#muokkaakentta textarea').value = "";
        muokkaa.style.display = 'block';
        lisaa.style.display = 'none';
    };

    //hides all operational fields at start
    muokkaa.style.display = 'none';
    muokkaakentta.style.display = 'none';
    lisaahenkilotiedot.style.display = 'none';

    //defines submit event for edit/add education/work xp for muokkaakenttaa-form
    muokkaakentta.onsubmit = function(){
        let date = document.querySelector('#muokkaakentta input:nth-Child(1)').value;
        let employer = document.querySelector('#muokkaakentta input:nth-Child(2)').value;
        let jobDescription = document.querySelector('#muokkaakentta textarea').value;
        let tableID = document.getElementById('TableID').value;

        let xhrPost = new XMLHttpRequest();
        xhrPost.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log(xhrPost.responseText);
            }
        };
        if(addNewRow == true){
            let data = {
                Date: date,
                Institution: employer,
                Description: jobDescription,
            };
            let jsonString = JSON.stringify(data);

            if(toTable == "degree"){
                xhrPost.open("POST", "http://localhost:80/NetCV/netcv/user/qualification/add/degrees", true);
                xhrPost.setRequestHeader("Content-type", "application/json");
                xhrPost.send(jsonString);
            }else if(toTable == "workexperience"){
                xhrPost.open("POST", "http://localhost:80/NetCV/netcv/user/qualification/add/workexperience", true);
                xhrPost.setRequestHeader("Content-type", "application/json");
                xhrPost.send(jsonString);
            }
        }else if(addNewRow == false){
            let data = {
                Date: date,
                Institution: employer,
                Description: jobDescription,
                TableID: tableID,
            };
            let jsonString = JSON.stringify(data);

            if(toTable == "degree"){
                console.log(toTable);
                xhrPost.open("POST", "http://localhost:80/NetCV/netcv/user/qualification/edit/degrees", true);
                xhrPost.setRequestHeader("Content-type", "application/json");
                xhrPost.send(jsonString);
            }else if(toTable == "workexperience"){
                console.log(toTable);
                xhrPost.open("POST", "http://localhost:80/NetCV/netcv/user/qualification/edit/workexperience", true);
                xhrPost.setRequestHeader("Content-type", "application/json");
                xhrPost.send(jsonString);
            }
        }
        updateAllListContent();
    };

    //defines submit event for personal information lisaahenkilotiedot-form
    document.getElementById('lisaahenkilotiedot').onsubmit = function() {
        let lastname = document.querySelector('#lisaahenkilotiedot input:nth-Child(1)').value;
        let name = document.querySelector('#lisaahenkilotiedot input:nth-Child(2)').value;
        let date = document.querySelector('#lisaahenkilotiedot input:nth-Child(3)').value;
        let email =document.querySelector('#lisaahenkilotiedot input:nth-Child(4)').value;
        let data = {
            LastName:lastname,
            FirstName:name,
            BirthDate:date ,
            Email:email,
        };
        let jsonstring = JSON.stringify(data);

        let xhrPost = new XMLHttpRequest();
        xhrPost.onreadystatechange = function(){
            if (this.readyState == 4 && this.status == 200) {
            }
        };
        if(jsonReceived == false){
            xhrPost.open("POST", "http://localhost:80/NetCV/netcv/user", true);
            xhrPost.setRequestHeader("Content-type", "application/json");
            xhrPost.send(jsonstring);
        }else{
            xhrPost.open("POST", "http://localhost:80/NetCV/netcv/user/update", true);
            xhrPost.setRequestHeader("Content-type", "application/json");
            xhrPost.send(jsonstring);
        }
        updateAllListContent();
    };
};