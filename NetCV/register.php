
<?php
include("php\serv_config.php");
session_start();

if($_SERVER["REQUEST_METHOD"] == "POST") {
    //Tähän tulee lomakkeelta tulleet arvot.

    $myusername = mysqli_real_escape_string($db,$_POST['accountname']);
    $mypassword = mysqli_real_escape_string($db,$_POST['password']);
    $password_hashed = password_hash($mypassword, PASSWORD_DEFAULT);
    $error = "";


    $sql = "SELECT UserID FROM account WHERE AccountName = '$myusername'";
    $result = mysqli_query($db,$sql);
    $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
    //$active = $row['active'];

    $count = mysqli_num_rows($result);

    // Jos löytyy käyttäjänimi, tulostetaan errori, muutoin luodaan tili.
    if($count == 1) {

       //$_SESSION['login_user'] = $myusername;
        $error = "Käyttäjä on jo olemassa.";
    }else {
        //kysytään tietokannasta montako käyttäjää ja määritetään käyttäjän tuleva id
        $sql = "Select UserID FROM account";
        $result = mysqli_query($db,$sql);
        $row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        $id = mysqli_num_rows($result) + 1;

        $sql = "INSERT INTO account (AccountName, Password, UserID) VALUES ('$myusername', '$password_hashed', $id)";

        if ($db->query($sql) === TRUE) {
            //$error = "Tili luotu onnistuneesti";
            header("location: login.php");

        } else {
            $error = "Virhe: " . $db->error;
        }
    }
}
?>

<html>

<head>
    <title>Luo uusi tili</title>

    <link href="./css/login_style.css" rel="stylesheet" type="text/css">

</head>

<body>
<h1 class="center">Net-CV</h1>
<div class="center">
    <div class="bigbox">
        <div class="labelbox"><b>Luo uusi Net-CV tili</b></div>

        <div  class="labelbox2">

            <form method = "post">
                <label class="label">Käyttäjänimi  :</label><input type = "text" class = "box" name = "accountname" /><br /><br />
                <label class="label">Salasana  :</label><input type = "password" class = "box" name = "password"  /><br/><br />
                <input type = "submit" value = " Luo tili "/>
                <input type="button" value="Kirjaudu sisään" onclick="window.location.href = 'login.php';"></input>
            </form>

            <div class="error">
                <?php

                echo $error; ?></div>

        </div>

    </div>

</div>