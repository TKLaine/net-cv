<?php
//Authors: Torsti Laine & Paavo Mattila
session_start();
include 'php/serv_config.php';
# URI parser helper functions
# ---------------------------

    function getResource() {
        # returns numerically indexed array of URI parts
        $resource_string = $_SERVER['REQUEST_URI'];
        if (strstr($resource_string, '?')) {
            $resource_string = substr($resource_string, 0, strpos($resource_string, '?'));
        }
        $resource = array();
        $resource = explode('/', $resource_string);
        array_shift($resource);
        array_shift($resource);
        //array_shift($resource);

        //print_r($resource);
        return $resource;
    }

    function getParameters() {
        # returns an associative array containing the parameters
        $resource = $_SERVER['REQUEST_URI'];
        $param_string = "";
        $param_array = array();
        if (strstr($resource, '?')) {
            # URI has parameters
            $param_string = substr($resource, strpos($resource, '?')+1);
            $parameters = explode('&', $param_string);
            foreach ($parameters as $single_parameter) {

                $param_name = substr($single_parameter, 0, strpos($single_parameter, '='));
                $param_value = substr($single_parameter, strpos($single_parameter, '=')+1);
                $param_array[$param_name] = $param_value;
            }
        }
        return $param_array;
    }

    function getMethod() {
        # returns a string containing the HTTP method
        $method = $_SERVER['REQUEST_METHOD'];
        return $method;
    }


# Handlers
# ------------------------------
# These are mock implementations

	function getUserCV(){
        #implements GET method for user's CV info
        #Example: GET  /netcv/user/
        global $db;
        $id = $_SESSION['myID'];
        if($db->connect_error){
            echo "Connection failed";
            die("Connection failed: ".$db->connect_error);
        };
        //obtain the names of the tables in database
        $sql = "SHOW TABLES";
        $result = mysqli_query($db, $sql);
        $table_names = array();
        $object = "";
        if(mysqli_num_rows($result)> 0){
            while($row=mysqli_fetch_array($result, MYSQLI_NUM)){
                array_push($table_names, $row[0]);
            };
        }
        $n = 0;
        foreach ($table_names as $table){

            $sql = "SELECT * FROM $table WHERE UserID='$id'";
            $sql_content_check = mysqli_query($db, $sql);

            if($table != "account" &&  mysqli_num_rows($sql_content_check) != 0){
                if($n == 0){
                    $object .= '{' . '"' . $table . '"' . ":";
                }else {
                    $object .= ', ' . '"' . $table . '"' . ":";
                }
                //obtain column names from the given table
                $sql = "DESCRIBE $table";
                $sql_table_query = mysqli_query($db, $sql);
                $table_columns = array();

                if(mysqli_num_rows($sql_table_query)> 0){
                    while($row=mysqli_fetch_array($sql_table_query, MYSQLI_NUM)){
                            array_push($table_columns, $row[0]);
                    };
                };

                //obtain the values of the table in question
                $sql_data_query = "SELECT * FROM $table WHERE UserID='$id'";
                $result_json = mysqli_query($db, $sql_data_query);
                $table_values = array();
                if(mysqli_num_rows($result_json)> 0){
                    while($row=mysqli_fetch_array($result_json, MYSQLI_NUM)){
                        foreach ($row as $item){
                                array_push($table_values, $item);
                        };
                    };
                };
                //create json object
                //forloop iterations
                $number_of_loops = count($table_values) / count($table_columns);
                $object .= '[';
                $k = 0;
                for($i = 0; $i < $number_of_loops; $i++){
                    $object .= '{';
                    for ($j = 0; $j < count($table_values); $j++) {
                        if($j == count($table_columns) - 1 && $i == $number_of_loops-1) {
                            $object .= '"' . $table_columns[$j] . '"' . ":" . '"' . $table_values[$k] . '"' . "}]";
                            $k++;
                        }else if ($j == count($table_columns) - 1) {
                            $object .= '"' . $table_columns[$j] . '"' . ":" . '"' . $table_values[$k] . '"' . "},";
                            $k++;
                        } else if($j < count($table_columns)) {
                            $object .= '"' . $table_columns[$j] . '"' . ":" . '"' . $table_values[$k] . '", ';
                            $k++;
                        }
                    }
                }
                $n++;
            }
        }
        $object .= "}";

        if($object == "}"){
            echo false;
        }else {
            echo $object;
        }
    }

    function getAccount(){
        #implements GET method for user account
        #Example: GET  /netcv/account?AccountName="user"&Password="user"
        global $db;
        header("Content-Type: application/json; charset=UTF-8");
        $data = json_decode(file_get_contents("php://input"), false);

        if($db->connect_error){
            echo "Connection failed";
            die("Connection failed: ".$db->connect_error);
        };

        $sql = "SELECT UserID FROM account WHERE AccountName = '$data->AccountName' and Password = '$data->Password'";
        $result = mysqli_query($db, $sql);
        //$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
        //$active = $row['active'];

        $count = mysqli_num_rows($result);

        // Jos löytyy rivi jossa kummatkin tiedot oikein, ohjataan käyttäjä rivilukumäärä=1 takia seuraavalle sivulle

        if($count == 1) {
            //session_register("myusername");
            //$_SESSION['login_user'] = $accountname;
            echo "ID löytyi";
            //header("location: preview.html");
        }else {
            echo "Sisäänkirjautuminen väärin, tarkista käyttäjätunnus ja salasana";
        }
    }

    function postUser(){
        #implements post method for user
        #Example: POST /netcv/user/encoded.json
        global $db;
        $id = $_SESSION['myID'];
        header("Content-Type: application/json; charset=UTF-8");
        $data = json_decode(file_get_contents("php://input"), false);
        if($db->connect_error){
            echo "Connection failed";
            die("Connection failed: ".$db->connect_error);
        }

        $sql = "INSERT INTO user (LastName, FirstName, BirthDate, Email, UserID)
            VALUES('$data->LastName', '$data->FirstName', '$data->BirthDate', '$data->Email', $id)";

        if($db->query($sql) === true){
            echo "Data delivery successful!";
        }else{
            echo "Data delivery failed: ".$sql."<br>".$db->error;
        }
    }

    function postUpdateUser(){
        #implements post method for user
        #Example: POST /netcv/user/update/encoded.json
	    global $db;
        $id = $_SESSION['myID'];
        header("Content-Type: application/json; charset=UTF-8");
        $data = json_decode(file_get_contents("php://input"), false);
        if($db->connect_error){
            echo "Connection failed";
            die("Connection failed: ".$db->connect_error);
        }
        $sql = "UPDATE user
                SET LastName = '$data->LastName', FirstName = '$data->FirstName', BirthDate = '$data->BirthDate', Email = '$data->Email'
                WHERE UserID = $id";

        if($db->query($sql) === true){
            echo "Data delivery successful!";
        }else{
            echo "Data delivery failed: ".$sql."<br>".$db->error;
        }
    }

    function postModifyQualifications($operation, $table){
        global $db;
        $id = $_SESSION['myID'];
        header("Content-Type: application/json; charset=UTF-8");
        $data = json_decode(file_get_contents("php://input", false));

        if($operation == "add"){
	        if ($table == "degrees"){
	            $sql = "INSERT INTO $table (RelevantDate, Institute, FreeDescription, UserID)
                        VALUES('$data->Date','$data->Institution','$data->Description', $id)";
            }else if($table == "workexperience"){
                $sql = "INSERT INTO $table (RelevantDate, Institute, FreeDescription, UserID)
                        VALUES('$data->Date','$data->Institution','$data->Description', $id)";;
            }
        }else if ($operation == "edit"){
            if ($table == "degrees"){
                $sql = "UPDATE $table
                        SET RelevantDate = '$data->Date', Institute = '$data->Institution', FreeDescription = '$data->Description'
                        WHERE TableID = $data->TableID";
            }else if($table == "workexperience"){
                $sql = "UPDATE $table
                SET RelevantDate='$data->Date', Institute='$data->Institution', FreeDescription='$data->Description'
                WHERE TableID=$data->TableID";
            }
        }

        if($db->query($sql) === true){
            echo "Data delivery successful!";
        }else{
            echo "Data delivery failed: ".$sql."<br>".$db->error;
        }

    }

# Main
# ----
	$resource = getResource();
    $request_method = getMethod();
    $parameters = getParameters();

    if ($resource[0]=="netcv") {
        if($request_method=="POST" && $resource[1]=="user" && $resource[2]=="update"){
            postUpdateUser();
        }else if($request_method=="POST" && $resource[1]=="user" && $resource[2]=="qualification"){
            postModifyQualifications($resource[3], $resource[4]);
        }
        else if ($request_method=="POST" && $resource[1]=="user") {
            postUser();
        }
        else if ($request_method=="GET" && $resource[1]=="user") {
            getUserCV();
        }
        else if ($request_method=="POST" && $resource[1]=="account") {
            getAccount($resource[1]);
        }
        else {
            http_response_code(405); # Method not allowed
        }
    }
    else {
        http_response_code(405); # Method not allowed
    }
?>

